//
//  CourierInvoiceSenderCell.swift
//  GoJekProvider
//
//  Created by Apple on 09/07/21.
//  Copyright © 2021 Appoets. All rights reserved.




import UIKit

class CourierInvoiceSenderCell: UITableViewCell {
    @IBOutlet weak var basefareView: UIView!
    @IBOutlet weak var distanceFareView: UIView!
    @IBOutlet weak var weightFareView: UIView!
    @IBOutlet weak var gstView: UIView!
    @IBOutlet weak var commissionView: UIView!
    @IBOutlet weak var totalDistanceView: UIView!
    @IBOutlet weak var promoCodeView: UIView!
    @IBOutlet weak var roundOffView: UIView!
    @IBOutlet weak var payableView: UIView!
    
    
    @IBOutlet weak var staticBaseFareLabel: UILabel!
    @IBOutlet weak var staticDistanceFareLabel: UILabel!
    @IBOutlet weak var staticWeightFareLabel: UILabel!
    @IBOutlet weak var staticGstLabel: UILabel!
    @IBOutlet weak var staticComLabel: UILabel!
    @IBOutlet weak var staticTotalDistanceLabel: UILabel!
    @IBOutlet weak var staticPromoCodeLabel: UILabel!
    @IBOutlet weak var staticRoundOffLabel: UILabel!
    @IBOutlet weak var staticPayableLabel: UILabel!
    
    
    @IBOutlet weak var baseeFareLabel: UILabel!
    @IBOutlet weak var distanceFareLabel: UILabel!
    @IBOutlet weak var weightFareLabel: UILabel!
    @IBOutlet weak var gstLabel: UILabel!
    @IBOutlet weak var comLabel: UILabel!
    @IBOutlet weak var totalDistanceLabel: UILabel!
    @IBOutlet weak var promoCodeLabel: UILabel!
    @IBOutlet weak var roundoffLabel: UILabel!
    @IBOutlet weak var payableLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initialLoads()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}


extension CourierInvoiceSenderCell{
    
    private func initialLoads(){
        
        setColors()
        setFont()
        setText()
        
    }
    
    
    private func setColors() {

        staticBaseFareLabel.textColor = .courierColor
        staticWeightFareLabel.textColor = .courierColor
        staticDistanceFareLabel.textColor = .courierColor
        staticGstLabel.textColor = .courierColor
        staticComLabel.textColor = .courierColor
        staticTotalDistanceLabel.textColor = .courierColor
        staticPromoCodeLabel.textColor = .courierColor
        staticRoundOffLabel.textColor = .courierColor
        staticPayableLabel.textColor = .darkGray

        baseeFareLabel.textColor = .darkGray
        weightFareLabel.textColor = .darkGray
        distanceFareLabel.textColor = .darkGray
        gstLabel.textColor = .darkGray
        comLabel.textColor = .darkGray
        totalDistanceLabel.textColor = .darkGray
        promoCodeLabel.textColor = .darkGray
        roundoffLabel.textColor =  .darkGray
        payableLabel.textColor = .darkGray
    }
    
    private func setText(){
        staticBaseFareLabel.text = TaxiConstant.baseFare
        staticWeightFareLabel.text = TaxiConstant.weightFare
        staticDistanceFareLabel.text = TaxiConstant.distanceFare
        staticGstLabel.text = TaxiConstant.taxFare
        staticComLabel.text = TaxiConstant.commission
        staticTotalDistanceLabel.text = TaxiConstant.totalDistance
        staticPromoCodeLabel.text = TaxiConstant.promoDiscount
        staticRoundOffLabel.text = TaxiConstant.roundOff
        staticPayableLabel.text = TaxiConstant.payable
    }
    
    
    private func setFont(){
        staticBaseFareLabel.font = .setCustomFont(name: .bold, size: .x16)
        baseeFareLabel.font = .setCustomFont(name: .bold, size: .x16)
        staticWeightFareLabel.font = .setCustomFont(name: .bold, size: .x16)
        weightFareLabel.font = .setCustomFont(name: .bold, size: .x16)
        staticDistanceFareLabel.font = .setCustomFont(name: .bold, size: .x16)
        distanceFareLabel.font = .setCustomFont(name: .bold, size: .x16)
        staticGstLabel.font = .setCustomFont(name: .bold, size: .x16)
        gstLabel.font = .setCustomFont(name: .bold, size: .x16)
        staticComLabel.font = .setCustomFont(name: .bold, size: .x16)
        comLabel.font = .setCustomFont(name: .bold, size: .x16)
        staticTotalDistanceLabel.font = .setCustomFont(name: .bold, size: .x16)
        totalDistanceLabel.font = .setCustomFont(name: .bold, size: .x16)
        staticPromoCodeLabel.font = .setCustomFont(name: .bold, size: .x16)
        promoCodeLabel.font = .setCustomFont(name: .bold, size: .x16)
        staticRoundOffLabel.font = .setCustomFont(name: .bold, size: .x16)
        roundoffLabel.font = .setCustomFont(name: .bold, size: .x16)
        staticPayableLabel.font = .setCustomFont(name: .bold, size: .x16)
        payableLabel.font = .setCustomFont(name: .bold, size: .x16)
    }
}
